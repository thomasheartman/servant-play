{ pkgs ? import <nixpkgs> {}, ghc ? pkgs.ghc }:
with pkgs;

stdenv.mkDerivation {
  name = "servant-play";
  inherit ghc;
  LANG="en_US.UTF-8";
  buildInputs = [ stack ];
}
