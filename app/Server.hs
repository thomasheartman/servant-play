{-# LANGUAGE DataKinds #-}



{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TypeOperators #-}

module Server
  ( app1
  , app2
  ) where

import Prelude ()
import Prelude.Compat

import Control.Monad.Except
import Control.Monad.Reader
import Data.Aeson.Compat
import qualified Data.Aeson.Parser
import Data.Aeson.Types
import Data.Attoparsec.ByteString
import Data.ByteString (ByteString)
import Data.List
import Data.Maybe
import Data.String.Conversions
import Data.Time.Calendar
import GHC.Generics
import Lucid
import Network.HTTP.Media ((//), (/:))
import Network.Wai
import Network.Wai.Handler.Warp
import Servant
import System.Directory
import Text.Blaze
import qualified Text.Blaze.Html
import Text.Blaze.Html.Renderer.Utf8

data User = User
  { name :: String
  , age :: Int
  , email :: String
  , registrationDate :: Day
  } deriving (Eq, Show, Generic)

instance ToJSON User

isaac :: User
isaac = User "Isaac Newton" 372 "isaac@newton.co.uk" (fromGregorian 1683 3 1)

albert :: User
albert = User "Albert Einstein" 136 "ae@mc2.org" (fromGregorian 1905 12 1)

users :: [User]
users = [ isaac , albert ]

type UserAPI1 = "users" :> Get '[ JSON] [User]
  :<|>  "albert" :> Get '[JSON] User
  :<|>  "isaac" :> Get '[JSON] User

server :: Server UserAPI1
server = return users
  :<|> return albert
  :<|> return isaac

userAPI :: Proxy UserAPI1
userAPI = Proxy

app1 :: Application
app1 = serve userAPI server


-- end users

type API = "position" :> Capture "x" Int :> Capture "y" Int :> Get '[JSON] Position
  :<|> "hello" :> QueryParam "name" String :> Get '[JSON] HelloMessage
  :<|> "marketing" :> ReqBody '[JSON] ClientInfo :> Post '[JSON] Email

data Position = Position
 { xCoord ::  Int
 , yCoord :: Int
 } deriving Generic

instance ToJSON Position

newtype HelloMessage = HelloMessage { msg :: String }
  deriving Generic

instance ToJSON HelloMessage

data ClientInfo = ClientInfo
  { clientName :: String
  , clientAge :: Int
  , clientEmail :: String
  , clientInterestedIn :: [String]
  } deriving Generic

instance FromJSON ClientInfo
instance ToJSON ClientInfo

data Email = Email
  { from :: String
  , to :: String
  , subject :: String
  , body :: String
  } deriving Generic

instance ToJSON Email

emailForClient :: ClientInfo -> Email
emailForClient c = Email from' to' subject' body'
  where from' = "great@company.com"
        to' = clientEmail c
        subject' = "Hey, " ++ clientName c ++ ", we miss you."
        body' = "Hi, " ++ clientName c ++ ",\n\n"
                ++ "since you recently turned " ++ show (clientAge c)
                ++ ", have you checked out our latest "
                ++ intercalate ", " (clientInterestedIn c)
                ++ " products? Give us a visit."

server2 :: Server API
server2 = position
  :<|> hello
  :<|> marketing

  where position x y = return (Position x y)

        hello mname = return . HelloMessage $ case mname of
          Nothing -> "Hello, anonymous coward."
          Just name -> "Hello, " ++ name

        marketing clientInfo = return (emailForClient clientInfo)

myAPI :: Proxy API
myAPI = Proxy

app2 :: Application
app2 = serve myAPI server2
